package com.nielskooij.graph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Node<T> {

	private List<SingleEdge<T>> edges = new ArrayList<>();
	private T data;
	
	public Node(T data){
		this.data = data;
	}
	
	public void addEdge(SingleEdge<T> edge){
		edges.add(edge);
	}
	
	public void removeEdge(SingleEdge<T> edge){
		edges.remove(edge);
	}
	
	public void removeEdge(Node<T> node){
		Iterator<SingleEdge<T>> iter = edges.iterator();
		
		while(iter.hasNext()){
			SingleEdge<T> edge = iter.next();
			
			if(edge.getEnd().equals(node)){
				iter.remove();
				return;
			}	
		}
	}
	
	public List<SingleEdge<T>> getEdges(){
		return edges;
	}
	
	public T getData(){
		return data;
	}
	
	public void setData(T data){
		this.data = data;
	}
	
}
