package com.nielskooij.graph;

import java.util.ArrayList;
import java.util.List;

public class Graph<T> {

	private List<Node<T>> nodes = new ArrayList<>();
	
	public Graph(){
		
	}
	
	public void addNode(Node<T> node){
		nodes.add(node);
	}
	
	public void removeNode(Node<T> node){
		nodes.remove(node);
	}
	
	public List<Node<T>> getNodes(){
		return nodes;
	}
	
}
